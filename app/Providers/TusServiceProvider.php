<?php

namespace App\Providers;

use App\Listeners\FileUploaded;
use Illuminate\Support\ServiceProvider;
use TusPhp\Tus\Server as TusServer;

class TusServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('tus-server', function ($app) {
            $server = new TusServer();

            $server->setApiPath('/tus')
                ->setUploadDir(storage_path('app/public/uploads'));

            $server->event()->addListener('tus-server.upload.created', [new FileUploaded, 'handle']);

            return $server;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
