<?php

namespace App\Jobs;

use App\Mail\ClientEmail;
use App\Mail\ProjectManagerEmail;
use App\UploadData;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmails implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new job instance.
     *
     * @param  UploadData  $data
     */
    public function __construct(UploadData $data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->data->project_manager->email)->send(new ProjectManagerEmail($this->data));

        Mail::to($this->data->email)->send(new ClientEmail($this->data));
    }
}
