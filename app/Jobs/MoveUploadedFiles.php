<?php

namespace App\Jobs;

use App\UploadData;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use RuntimeException;

class MoveUploadedFiles implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $uploadData;

    /**
     * Create a new job instance.
     *
     * @param  UploadData  $uploadData
     */
    public function __construct(UploadData $uploadData)
    {
        $this->uploadData = $uploadData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $dir = storage_path('app/public/'.Str::slug($this->uploadData->company_name.'-'.$this->uploadData->project_name.'-'.now()->toDateString()));

        if (
            !file_exists($dir) &&
            !mkdir($concurrentDirectory = $dir) &&
            !is_dir($concurrentDirectory)
        ) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }

        foreach ($this->uploadData->files as $file) {
            $newFilePath = $dir.'/'.$file->name;

            rename(storage_path('app/public/uploads/'.$file->name), $newFilePath);

            $file->file_path = $newFilePath;

            $file->location = asset(Storage::url(Str::after($newFilePath, '/var/www/storage/app/public/')));

            $file->save();
        }
    }
}
