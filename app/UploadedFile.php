<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UploadedFile extends Model
{
    protected $table = 'fileuploads';

    protected $fillable = [
        'upload_data_id',
        'file_id',
        'name',
        'size',
        'offset',
        'location',
        'file_path',
    ];

    public function data()
    {
        return $this->belongsTo(UploadData::class, 'upload_data_id');
    }
}
