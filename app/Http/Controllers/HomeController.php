<?php

namespace App\Http\Controllers;

use App\Jobs\MoveUploadedFiles;
use App\Jobs\SendEmails;
use App\ProjectManager;
use App\UploadData;
use App\UploadedFile;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $projectManagers = ProjectManager::all();

        return view('home', compact('projectManagers'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'company_name' => 'required',
            'project_name' => 'required',
            'project_manager_id' => 'required|exists:project_managers,id',
            'uppyResult' => 'required|json',
            'g-recaptcha-response' => 'required|captcha'
        ]);

        $uploadData = UploadData::create($request->all());

        $uppyResult = json_decode($request->get('uppyResult'), true);

        foreach ($uppyResult['successful'] as $fileData) {
            $fileId = $fileData['meta']['file_id'];

            $uploadedFile = UploadedFile::where('file_id', $fileId)->first();

            if ($uploadedFile) {
                $uploadedFile->offset = $uploadedFile->size;

                $uploadedFile->upload_data_id = $uploadData->id;

                $uploadedFile->save();
            }
        }

        foreach ($uppyResult['failed'] as $fileData) {
            $fileId = $fileData['meta']['file_id'];

            UploadedFile::where('file_id', $fileId)->delete();
        }

        MoveUploadedFiles::withChain([
            new SendEmails($uploadData)
        ])->dispatch($uploadData);

        return redirect('/')->with('status', 'Data saved!');
    }
}
