<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectManager extends Model
{
    public function upload_data()
    {
        return $this->hasMany(UploadData::class);
    }
}
