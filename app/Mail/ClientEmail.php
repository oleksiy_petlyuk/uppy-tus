<?php

namespace App\Mail;

use App\UploadData;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ClientEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $uploadData;

    /**
     * Create a new message instance.
     *
     * @param  UploadData  $uploadData
     */
    public function __construct(UploadData $uploadData)
    {
        $this->uploadData = $uploadData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Your files were successfully uploaded')
            ->view('emails.for_client');
    }
}
