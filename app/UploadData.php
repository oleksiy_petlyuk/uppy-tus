<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UploadData extends Model
{
    protected $fillable = [
        'email',
        'first_name',
        'last_name',
        'company_name',
        'project_name',
        'project_manager_id',
        'message',
    ];

    public function files()
    {
        return $this->hasMany(UploadedFile::class);
    }

    public function project_manager()
    {
        return $this->belongsTo(ProjectManager::class);
    }
}
