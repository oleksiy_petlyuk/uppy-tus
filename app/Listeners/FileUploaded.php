<?php

namespace App\Listeners;

use App\UploadedFile;
use TusPhp\Events\TusEvent;

class FileUploaded
{
    /**
     * Handle the event.
     *
     * @param  TusEvent  $event
     * @return void
     */
    public function handle(TusEvent $event)
    {
        $request = $event->getRequest();

        $fileMeta = $event->getFile()->details();

        UploadedFile::create([
            'file_id' => $request->extractMeta('file_id'),
            'name' => $fileMeta['name'],
            'size' => $fileMeta['size'],
            'offset' => $fileMeta['offset'],
            'location' => $fileMeta['location'],
            'file_path' => $fileMeta['file_path'],
        ]);
    }
}
