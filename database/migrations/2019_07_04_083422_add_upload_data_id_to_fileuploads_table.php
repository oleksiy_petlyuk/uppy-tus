<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUploadDataIdToFileuploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fileuploads', function (Blueprint $table) {
            $table->unsignedBigInteger('upload_data_id')->nullable()->after('id')->nullable();
            $table->string('file_id')->after('upload_data_id');
            $table->unsignedBigInteger('offset')->after('size');
            $table->string('location')->after('offset');
            $table->string('file_path')->after('location');

            $table->foreign('upload_data_id')->references('id')->on('upload_data')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fileuploads', function (Blueprint $table) {
            $table->dropForeign(['upload_data_id']);

            $table->dropColumn('upload_data_id', 'file_id', 'offset', 'location', 'file_path');
        });
    }
}
