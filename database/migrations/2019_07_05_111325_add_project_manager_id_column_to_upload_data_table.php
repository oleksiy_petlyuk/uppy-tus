<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProjectManagerIdColumnToUploadDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('upload_data', function (Blueprint $table) {
            $table->unsignedBigInteger('project_manager_id')->after('project_name');
            $table->string('email')->after('id');

            $table->dropColumn('project_manager_name');

            $table->foreign('project_manager_id')->references('id')->on('project_managers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('upload_data', function (Blueprint $table) {
            $table->string('project_manager_name')->after('project_name');

            $table->dropForeign(['project_manager_id']);

            $table->dropColumn('project_manager_id', 'email');
        });
    }
}
