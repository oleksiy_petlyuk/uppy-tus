<?php

use App\ProjectManager;
use Illuminate\Database\Seeder;

class ProjectManagersTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(ProjectManager::class, 10)->create();
    }
}
