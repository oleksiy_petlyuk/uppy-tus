const Uppy = require('@uppy/core')
const Dashboard = require('@uppy/dashboard')
const Tus = require('@uppy/tus')
const Form = require('@uppy/form')

const uppy = Uppy({
    restrictions: {
        maxFileSize: 1e+10, // 10gb
        maxNumberOfFiles: 3,
        minNumberOfFiles: 1,
        allowedFileTypes: ['image/*', 'video/*']
    }
})
    .use(Dashboard, {
        inline: true,
        target: '#drag-drop-area',
        replaceTargetContent: true,
        showProgressDetails: true,
        note: 'Images and video only, 1–3 files, up to 10 GB',
        height: 470,
        showLinkToFileUploadResult: false,
        hideUploadButton: true
    })

    .use(Tus, {
        endpoint: '/tus',
        resume: true,
        autoRetry: true,
        retryDelays: [0, 1000, 3000, 5000]
    })

    .use(Form, {
        target: '#upload_form',
        multipleResults: true,
        triggerUploadOnSubmit: true,
        submitOnSuccess: true,
    })

uppy.on('file-added', (file) => {
    const fileId = localStorage.getItem(file.id) || randomString()

    localStorage.setItem(file.id, fileId)

    uppy.setFileMeta(file.id, {file_id: fileId})
})


const randomString = () => {
    return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
}
